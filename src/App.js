import React, { Component } from 'react';
import { Layout, Header, Navigation, Drawer, Content, Footer, FooterSection, FooterLinkList } from 'react-mdl';
import { Link } from 'react-router-dom';
import Main from './components/main';
//import logo from './logo.svg';
import './sass/App.scss';

class App extends Component {
	render () {
		return(
			<div className="demo-big-content">
					<Layout>
							<Header className="header-color" title={<Link style={{textDecoration: 'none', color: 'white'}}
							to="/">Portfolio (site en cours)</Link>} scroll>
									<Navigation>
											<Link to="/cv">CV</Link>
											<Link to="/apropos">A propos</Link>
											<Link to="/projects">Projets</Link>
											<Link to="/contact">Contact</Link>
											<Link to="/contact">Authorized form</Link>
									</Navigation>
							</Header>
							<Drawer title={<Link style={{textDecoration: 'none', color: 'black'}}
							to="/">Portfolio (site en cours)</Link>}>
									<Navigation>
										<Link to="/cv">CV</Link>
										<Link to="/apropos">A propos</Link>
										<Link to="/projects">Projets</Link>
										<Link to="/contact">Contact</Link>
									</Navigation>
							</Drawer>
							<Content>
									{/* <div className="page-content" /> */}
									<Main/>
							</Content>
					</Layout>
					{/* <Footer id="footer" size="mini">
						<FooterSection type="left" logo="Title">
								<FooterLinkList>
										<Link to="/">Accueil</Link>
										<a href="/">Help</a>
										<a href="/">Privacy & Terms</a>
								</FooterLinkList>
						</FooterSection>
				</Footer> */}
			</div>
		)
		
	}
}

export default App;
