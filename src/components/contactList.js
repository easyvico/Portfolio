import React from "react";
// import the Contact component
import Contact2 from "./contact2";

function ContactList(props) {
  return (
    <div>
      {props.tabContacts.map(c => <Contact2 key={c.id} name={c.name} />)}
     </div> 
  ); 
} 

export default ContactList;