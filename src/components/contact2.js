import React from 'react';
import PropTypes from "prop-types";

function Contact2(props) {
	return (
    <div className="contact">
      <span>{props.name}</span>
    </div>
  );
}

/* Pour plus de sécurité, nous définissons que la prop 'name' est de type string et que c'est obligatoire. 
Si vous passez quelque chose qui n'est pas une chaîne (string), ou ne fournissez rien pour cette prop, vous obtiendrez une erreur dans la console de votre navigateur. */
Contact2.propTypes = {
  name: PropTypes.string.isRequired
};

export default Contact2;