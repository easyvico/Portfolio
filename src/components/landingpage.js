import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';
//import IcoMoon from 'react-icomoon';

class Landingpage extends Component {
	render() {
		return(
			<div>				
				<Grid className="image-descriptif" style={{justifyContent: 'center'}}>
					<Cell col={6} phone={4}>					
						<img
							src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSneerB5Acsvq15wTsbnnPSsmr72u4TrE2OfKVTeSgIbDlD3Y7RMw"
							alt="avatar"
							className="avatar-img"
						/>
						<div className="banner-text">
							{/* Icomoon test :
							<IcoMoon icon="plane" style={{fontSize: '20px', color: 'red', marginRight: '20px'}} />
							<span>test hello ca va ?</span> */}
							<h1>Developpeur front-end</h1>
							<hr/>
							<p>Html / Css | Bootstrap | Javascript | React</p>
							<div className="social-links">
								{/* Linkedin */}
								<a href="http://google.com" rel="noopener noreferrer" target="_blank">
									<i className="fa fa-linkedin-square" aria-hidden="true"></i>
								</a>
								{/* Github */}
								<a href="http://google.com" rel="noopener noreferrer" target="_blank">
									<i className="fa fa-github-square" aria-hidden="true"></i>
								</a>
								{/* Youtube */}
								<a href="http://google.com" rel="noopener noreferrer" target="_blank">
									<i className="fa fa-youtube-square" aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</Cell>
				</Grid>
			</div>
		)
	}
}

export default Landingpage;