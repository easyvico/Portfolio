import React, { Component } from 'react';
import { Tabs, Tab, Grid, Cell } from 'react-mdl';
import CardList from './cardList';
import image_1 from '../img/bg.jpg';
import image_2 from '../img/logo-react.png';
//import image_3 from '../img/bg.jpg';
//import data from '../data.json';

class Projects extends Component {
	constructor(props) {
		super(props);
		this.state = {
			activeTab: 1,
			count: 0
		};
	}

	// méthode qui switch sur la bonne catégorie et retourne REACT cat ou HTML cat
	toggleCategories() {
		/* Tableau d'objects complexe représentant une liste et son id */
		const tabCard = [
			{ id: 1,
				titre: "Application météo",
				name: "Demo", 
				url: "/apropos",
				projetImage: image_1
			},
			{ id: 2, 
				titre: "Projet 2",
				name: "Demo 2",
				url: "/apropos",
				projetImage: image_2
			},
			{ id: 3, 
				titre: "Projet 3",
				name: "Demo 3" ,
				url: "/apropos",
				projetImage: image_2
			}
		];
		if(this.state.activeTab === 0) {
			return(
				/* REACT Catégorie */
				<div className="projects-grid mdl-grid">
					{/* on passe ds le composant un tableau 'tabCard' ayant pour nom de prop 'tabCard' */}
					<CardList tabCard={tabCard} />
				</div>
			)
		}
		else if(this.state.activeTab === 1) {
			return(
				/* HTML Catégorie */
				<div>HTML/CSS</div>
			)
		}
	}
	// fonction qui met à jour le state
	countClick() {
		this.setState(
			{
				count: this.state.count + 1
			}
		)
	}

	render() {
		return(
			<div className="category-tabs">
				{/* Exemple pour incrémenter une valeur */}
				{/* <button onClick={() => this.countClick()}>Click ici</button>
				<p>{this.state.count}</p> */}

				<Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} ripple>
						<Tab>React</Tab>
						<Tab>HTML/CSS</Tab>
				</Tabs>
				<section>
						<Grid>
							<Cell col={12}>
								<div className="">{this.toggleCategories()}</div>
							</Cell>
						</Grid>
				</section>
			</div>
		)
	}
}

export default Projects;