import React from 'react';
import { Card, CardTitle, CardText, CardActions } from 'react-mdl';

const Card1 = props => (
			<Card 
				shadow={5} 
				style={{minWidth: '400px', margin: '0 auto 30px auto'}}
			>
				<CardTitle style={ { color: '#fff', height: '176px', background: `url(${props.image}) no-repeat center center` } }>
				{props.titre}
				</CardTitle>
				<CardText>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 		labore et dolore magna aliqua.
				</CardText>
				<CardActions>
					<a class="mdl-button mdl-button--colored" 
						style={{ textDecoration: 'none' }} 
						href={props.links}>{props.name}
					</a>
				</CardActions>
			</Card>

)

export default Card1;