import React from "react";
// import the Contact component
import Card1 from "./card1";

const CardList = (props) => (
  props.tabCard.map(objet => <Card1 key={objet.id} titre={objet.titre} name={objet.name} links={objet.url} image={objet.projetImage}/>)
)

export default CardList;