import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';
import Education from './education'
import Experience from './experience'
import Skills from './skills';
//import ContactList from './contactList';

/* Tableau d'objects complexe représentant une liste et son id */
// const contacts = [
//   { id: 1, name: "Leanne Graham" },
//   { id: 2, name: "Ervin Howell" },
//   { id: 3, name: "Clementine Bauch" },
//   { id: 4, name: "Patricia Lebsack" }
// ];

class Resume extends Component {
	render() {
		return(
			<div className="">
				<Grid>
					<Cell col={4}>
						<div style={{textAlign: 'center'}}>
						<img 	src="https://cdn0.iconfinder.com/data/icons/user-pictures/100/male-512.png" 
									alt="avatar"
									style={{height: '200px'}}
							/>
						</div>
						<h2 style={{paddingTop: '2em'}}>Laurent Vicherd</h2>
						<h4 style={{color: 'grey'}}>Développeur</h4>
						<hr style={{borderTop: '3px solid #e22947', width: '50%'}}/>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
						</p>
						<hr style={{borderTop: '3px solid #e22947', width: '50%'}}/>
						<h5>Mobile</h5>
						<p>06 71 72 38 92</p>
						<h5>Email</h5>
						<p>laurent.vic@hotmail.fr</p>
						<h5>Web</h5>
						<p>laurentvicherd.com</p>
						<hr style={{borderTop: '3px solid #e22947', width: '50%'}}/>
					</Cell>

					<Cell className="resume-right-col" col={8}>
						<h2>Education</h2>
						<Education 
							startYear={2017}
							endYear={2018}
							societyName={"MAIF"}
							societyDescription={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."}
						/>
						<Education 
							startYear={2016}
							endYear={2017}
							societyName={"Groupe Rhinos"}
							societyDescription={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."}
						/>

						<hr style={{borderTop: '3px solid #e22947'}}/>
						<h2>Experience</h2>
						<Experience
							startYear={2016}
							endYear={2017}
							jobName={"Job1"}
							jobDescription={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."}
						/>

						<hr style={{borderTop: '3px solid #e22947'}}/>
						<h2>Skills</h2>
						<Skills 
							skill="javascript"
							progress={60}
						/>
						<Skills 
							skill="HTML/CSS"
							progress={80}
						/>
						<Skills 
							skill="Bootstrap"
							progress={70}
						/>
						<Skills 
							skill="React"
							progress={50}
						/>

						{/* on passe ds le composant un tableau 'contacts' ayant pour nom de prop 'tabContacts' */}
						{/* <ContactList tabContacts={contacts} /> */}
					</Cell>
				</Grid>	

			</div>
		)
	}
}

export default Resume;